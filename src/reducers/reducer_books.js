export default function() {
  return [
    { title: 'JavaScript: The Good Parts', pages: 101, images: "../img/javascript-books.jpg" },
    { title: 'Harry Potter', pages: 39, images: "../img/harry-potter.jpg" },
    { title: 'The Dark Tower', pages: 85, images: "../img/the-darktower.jpg" },
    { title: 'Eloquent Ruby', pages: 1, images: "../img/eloquent-ruby.jpg" }
  ];
}
