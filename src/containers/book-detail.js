import React, { Component } from 'react';
import { connect } from 'react-redux';


class  BookDetail extends Component {
  render() {
    if(!this.props.book) {
      return <div className="select-book">Select a book to get started.</div>
    }

    return (
      <div className="book-detail">
        <h3> Details for:</h3>
        <div>Title: {this.props.book.title}</div>
        <div>Pages: {this.props.book.pages}</div>
        <img className="col-sm-4 book-images" src={this.props.book.images} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    book: state.activeBook
  };
}

export default connect (mapStateToProps)(BookDetail);
